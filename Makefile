PROM_OPERATOR_URL = https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/release-0.49/bundle.yaml

UNAME := $(shell uname -m)

NORMAL_COLOR = \033[0m
BOLD = \033[1m
RED = \033[31m
GREEN = \033[32m
YELLOW = \033[33m
BLUE = \033[34m
MAGENTA = \033[35m
CYAN = \033[36m

.PHONY: help
.PHONY: up
.PHONY: down
.PHONY: restart


help:
	@printf '\n Deploy multi-cluster prometheus & thanos lab environment\n Requires kind\n'
	@printf '\n  Usage:\n'
	@printf '    make %b<target>%b\n\n' '$(MAGENTA)' '$(NORMAL_COLOR)'
	@printf '  Targets:\n'
	@printf '    %bup%b        bring up test cluster, local registry, and dummy app for HPA testing\n' '$(BLUE)' '$(NORMAL_COLOR)'
	@printf '    %bdown%b      Bring down test cluster and registry\n' '$(BLUE)' '$(NORMAL_COLOR)'
	@printf '    %brestart%b   bring cluster down then up again\n\n' '$(BLUE)' '$(NORMAL_COLOR)'


up:
	@printf '%b%b==>%b%b Creating cluster & registry...%b\n' '$(BOLD)' '$(CYAN)' '$(NORMAL_COLOR)' '$(GREEN)' '$(NORMAL_COLOR)'
	@./util/setup_cluster.sh
	@printf '\n'
	@printf '%b%b==>%b%b Deploying Prometheus operator...%b\n' '$(BOLD)' '$(CYAN)' '$(NORMAL_COLOR)' '$(GREEN)' '$(NORMAL_COLOR)'
	@curl -sL '$(PROM_OPERATOR_URL)' | kubectl --context kind-hpa-test apply -f -
	@printf '%b%b==>%b%b Deploying Prometheus adapter...%b\n' '$(BOLD)' '$(CYAN)' '$(NORMAL_COLOR)' '$(GREEN)' '$(NORMAL_COLOR)'
	@./util/gencerts.sh
	@kubectl --context kind-hpa-test apply -f k8s/adapter/common
	@kubectl --context kind-hpa-test apply -f k8s/adapter/$(UNAME)
	@printf '\n'
	@printf '%b%b==>%b%b Building exporter container...%b\n' '$(BOLD)' '$(CYAN)' '$(NORMAL_COLOR)' '$(GREEN)' '$(NORMAL_COLOR)'
	@docker build -t localhost:5000/hpa_exporter dummy_exporter/
	@docker push localhost:5000/hpa_exporter
	@printf '\n'
	@printf '%b%b==>%b%b Deploying k8s resources...%b\n' '$(BOLD)' '$(CYAN)' '$(NORMAL_COLOR)' '$(GREEN)' '$(NORMAL_COLOR)'
	@kubectl --context kind-hpa-test apply -f k8s/prom
	@printf '\n'

down:
	@printf '%b%b==>%b%b Deleting cluster...%b\n' '$(BOLD)' '$(RED)' '$(NORMAL_COLOR)' '$(YELLOW)' '$(NORMAL_COLOR)'
	@kind delete cluster --name hpa-test
	@printf '\n'
	@printf '%b%b==>%b%b Deleting registry...%b\n' '$(BOLD)' '$(RED)' '$(NORMAL_COLOR)' '$(YELLOW)' '$(NORMAL_COLOR)'
	@docker rm -fv kind-registry || true
	@printf '\n'
	@printf '%b%b==>%b%b Deleting exporter image...%b\n' '$(BOLD)' '$(RED)' '$(NORMAL_COLOR)' '$(YELLOW)' '$(NORMAL_COLOR)'
	@docker rmi localhost:5000/hpa_exporter || true
	@printf '\n\n'

restart: down up
