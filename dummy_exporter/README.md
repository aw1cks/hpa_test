# HPA exporter test

This python program creates a prometheus exporter which exposes the metric `dummy_memory_pressure` which can be changed to trigger HPA events.

The included Dockerfile will build a container for this application, exposing the program on port 5000.

## Usage

```shell
$ docker build -t hpa_exporter .
$ docker run --rm -p 5000:5000 -ti hpa_exporter
# See current value of our metric
$ curl 127.0.0.1:5000/metrics | tail -1
# Update value and check it again
$ curl 127.0.0.1:5000/setMetric/12
$ curl 127.0.0.1:5000/metrics | tail -1
```
