#!/usr/bin/env python3

from flask import Flask
from werkzeug.middleware.dispatcher import DispatcherMiddleware
from prometheus_client import Gauge, make_wsgi_app

DUMMY_MEMORY_PRESSURE = Gauge(
    "dummy_memory_pressure",
    "Memory pressure for my application"
)
DUMMY_MEMORY_PRESSURE.set(1)


app = Flask(__name__)
app.wsgi_app = DispatcherMiddleware(
    app.wsgi_app,
    {
        "/metrics": make_wsgi_app()
    }
)

@app.route("/")
def default():
    return "Nothing to see here\nCheck out the /metrics endpoint\n"

@app.route("/setMetric/<value>")
def setMetric(value):
    global DUMMY_MEMORY_PRESSURE
    try:
        DUMMY_MEMORY_PRESSURE.set(value)
        return "Metric value set to {}\n".format(value)
    except ValueError:
        return "Invalid data\n", 422
