# HorizontalPodAutoscaler Lab

Spins up a `kind` cluster, then deploys the Prometheus operator
and a Prometheus instance \
(exposed on host port `30001`).

The dummy application under `dummy_exporter` is built as a container
and pushed to a local registry, \
which is then deployed into the cluster.

This application is exposed on host port `30000`, \
which presents an API endpoint to control the metrics it serves - \
see the documentation under the `dummy_exporter` folder.

Prometheus is configured to scrape the metrics presented by the application, \
which in turn is used by the Prometheus adapter to enable Pod auto scaling.

## Requirements

 - `make`
 - `docker`
 - `kind`
 - `jq`
 - `openssl`
 - `cfssl`

## Usage

```shell
# Bring up the cluster
$ make up
# Wait until the metrics are seen by the HPA
$ kubectl describe hpa -w
# Change our metric value to cause a scaling event
$ curl 127.0.0.1:30000/setMetric/2
# Check for a scaling event... this may take a little bit
$ kubectl describe hpa -w
```
